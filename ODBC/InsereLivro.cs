﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Odbc;

namespace ODBC
{
    class InsereLivro
    {
        static void Main(string args)
        {
            System.Console.Write("Digite o Título do Livro:");
            string titulo = System.Console.ReadLine();

            System.Console.Write("Digite o Preço do Livro:");
            string preco = System.Console.ReadLine();

            System.Console.Write("Digite o Id da Editora do Livro:");
            string editoraId = System.Console.ReadLine();

            string textoInsereLivro = @"INSERT INTO Livros (Titulo, Preco, EditoraId) VALUES (?, ?, ?)";

            using (OdbcConnection conexao = ConnectionFactory.CreateConnection())
            {
                OdbcCommand command = new OdbcCommand(textoInsereLivro, conexao);

                command.Parameters.AddWithValue("@Titulo", titulo);
                command.Parameters.AddWithValue("@Preco", preco);
                command.Parameters.AddWithValue("@EditoraId", editoraId);

                conexao.Open();
                command.ExecuteNonQuery();
            }
        }
    }
}
