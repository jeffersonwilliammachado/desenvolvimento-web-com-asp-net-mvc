﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Odbc;

namespace ODBC
{
    static class ConnectionFactory
    {
        public static OdbcConnection CreateConnection()
        {
            string driver = @"SQL Server";
            string servidor = @"JEFFERSON";
            string baseDeDados = @"livraria";
            string usuario = @"sa";
            string senha = @"123";

            StringBuilder connectionString = new StringBuilder();

            connectionString.Append("Driver=");
            connectionString.Append(driver);
            connectionString.Append("; Server=");
            connectionString.Append(servidor);
            connectionString.Append("; Database=");
            connectionString.Append(baseDeDados);
            connectionString.Append("; uid=");
            connectionString.Append(usuario);
            connectionString.Append("; pwd=");
            connectionString.Append(senha);

            return new OdbcConnection(connectionString.ToString());
        }
    }
}
