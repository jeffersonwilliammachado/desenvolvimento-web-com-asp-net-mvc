﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Odbc;

namespace ODBC
{
    class ListaEditora
    {
        static void Main(string[] args)
        {
            using (OdbcConnection conexao = ConnectionFactory.CreateConnection())
            {
                string textoListaEditora = @"SELECT * FROM Editoras";

                OdbcCommand command = new OdbcCommand(textoListaEditora, conexao);
                conexao.Open();
                OdbcDataReader resultado = command.ExecuteReader();

                while (resultado.Read())
                {
                    long? id = resultado["Id"] as long?;
                    string nome = resultado["Nome"] as string;
                    string email = resultado["Email"] as string;

                    System.Console.WriteLine("{0} : {1} - {2}", id, nome, email);
                }

                System.Console.ReadLine();
            }
        }
    }
}
