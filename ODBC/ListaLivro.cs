﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Odbc;

namespace ODBC
{
    class ListaLivro
    {
        static void Main(string args)
        {
            using (OdbcConnection conexao = ConnectionFactory.CreateConnection())
            {
                string textoListaLivro = @"SELECT * FROM Livros";

                OdbcCommand command = new OdbcCommand(textoListaLivro, conexao);
                conexao.Open();
                OdbcDataReader resultado = command.ExecuteReader();

                while (resultado.Read())
                {
                    long? id = resultado["Id"] as long?;
                    string titulo = resultado["Titulo"] as string;
                    float? preco = resultado["Preco"] as float?;
                    long? editoraId = resultado["EditoraId"] as long?;

                    System.Console.WriteLine("{0} : {1} - {2} - {3}", id, titulo, preco, editoraId);
                }

                System.Console.ReadLine();
            }
        }
    }
}
