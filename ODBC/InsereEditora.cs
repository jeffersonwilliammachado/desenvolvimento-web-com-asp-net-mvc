﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Odbc;

namespace ODBC
{
    class InsereEditora
    {
        static void Main(string args)
        {
            System.Console.Write("Digite o Nome da Editora:");
            string nome = System.Console.ReadLine();

            System.Console.Write("Digite o Email da Editora:");
            string email = System.Console.ReadLine();

            string textoInsereEditora = @"INSERT INTO Editoras (Nome, Email) VALUES (?, ?)";

            using (OdbcConnection conexao = ConnectionFactory.CreateConnection())
            {
                OdbcCommand command = new OdbcCommand(textoInsereEditora, conexao);

                command.Parameters.AddWithValue("@Nome", nome);
                command.Parameters.AddWithValue("@Email", email);

                conexao.Open();
                command.ExecuteNonQuery();
            }
        }
    }
}
